from tkinter import *
from tkinter import font

class Register:
	
	def __init__(self):
		# self.usuario = usuario
		# print(self.usuario)
		self.r= Tk()
		self.r.overrideredirect(True)
		self.r.geometry("420x360+482+250")
		self.r.title("Registro exitoso")
		self.r.resizable(0,0)
		self.r.iconbitmap ("src/correc.ico")#Icono

	def Iniciar(self,control,usuario):
		self.usuario = usuario
		self.con = control
		self.frame= Frame ()
		self.frame.place(x=0)
		self.frame.config(bg="#F4B183")
		self.frame.config(width="420", height="180")

		self.frameima= Frame ()
		self.frameima.place(x=0,y= 190)
		self.frameima.config(bg="#CDC1C5")
		self.frameima.config()
		self.ima = PhotoImage (file = "src/Log.png")

		Helvfont = font.Font(family="Helvetica", size = 30)

		Label(self.r, text="Registro\nExitoso",font = Helvfont, relief =FLAT,bg="#F4B183", fg="#FAFAFA").place(x="140",y="50")
		b1 = Button (self.r, image=self.ima,relief= FLAT)
		b1.bind('<Button-1>', self.cerrar)
		b1.place(x="132",y="180")

		self.r.mainloop()

	def cerrar(self,event):
		# pass
		self.r.destroy()
		self.con.setAccion("Chat",self.usuario)