from tkinter import *

class Mover():

    def __init__(self):

        self.root = Tk()
        self.root.geometry('500x500+100+100')

        self.image1 = PhotoImage(file = '../../src/email.png')

        self.canvas = Canvas(self.root, width = 500, height = 400, bg = 'white')
        self.canvas.pack()
        self.imageFinal = self.canvas.create_image(300, 300, image = self.image1)

    def move(self):

        self.canvas.move(self.imageFinal, 0, 22)  
        self.canvas.update()

        self.button = Button(text = 'move', height = 3, width = 10, command = self.move)
        self.button.pack(side = 'bottom', padx = 5, pady = 5)

        self.root.mainloop()
        
a = Mover()
a.move()