from tkinter import *
from tkinter import font
from app.Controlador.controlchat.configuracion.configuracion import configuracion

class configurar():
    def __init__ (self,usuario,vista):

        self.usuario = usuario
        self.conf = Toplevel(vista,bg="#F2F2F2")
        self.conf.resizable(0,0)
        self.conf.title ("Configuracion")
        self.conf.geometry("280x480+500+200")
        self.conf.config(bg="#F2F2F2")
        self.ima = PhotoImage (file = "src/perfil.png")
        self.emailUser = StringVar()
        self.userName = StringVar()
        self.contrUser=StringVar()
        self.configuracion = configuracion()

    def Iniciar(self,control):

        self.configuracion.setValues(self)
        self.control = control

        Helvfont = font.Font(family="Helvetica", size = 14)
        Hel = font.Font(family="Arial", size = 14)

        Label (self.conf, image=self.ima,relief= FLAT, bg="#F2F2F2").place(x="95",y="15")
       	Label (self.conf, text = "Correo electronico", bg = "#F2F2F2", font = Helvfont).place(x= 65, y= 125)
        Entry (self.conf, relief = SOLID, font=Hel).place(x=40,y=160, width= 215, height=30)

        Label (self.conf, text = "Nombre de usuario", bg = "#F2F2F2", font = Helvfont).place(x= 65, y= 220) 	
       	Entry (self.conf, relief = SOLID, font=Hel).place (x=40, y = 255,width = 215, height=30)

        Label (self.conf, text = "Contraseña", bg = "#F2F2F2", font = Helvfont).place(x= 85, y= 320)
        Entry(self.conf, textvariable=self.contrUser,relief = SOLID, font = Hel).place (x= 40, y=355, width=215, height=30)


       	b1=Button (self.conf, text = "Enviar", font= Hel,cursor = "hand2", fg= "#0F1419", relief = FLAT)
        b1.bind("<Button-1>",self.modificar)
        b1.place (x=65, y=410, width = 150, height=35)
        
        self.conf.mainloop()
    
    def cerrar(self,event):
        self.conf.destroy()
    
    def modificar(self,event): 
    
        self.configuracion.modificar(self.emailUser.get(),self.userName.get(),self.contrUser.get(),self.usuario)
        self.conf.destroy()
        self.control.setAccion("Chat",self.usuario)
