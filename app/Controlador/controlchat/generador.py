import random
import hashlib
import pyperclip

class codicop:

  def __init__(self,nombreGrupo):
    self.nomgrupo = nombreGrupo
    self.CodGrupo = hashlib.md5(self.nomgrupo.encode()).hexdigest()[0:2]

  numeroaz= lambda self: str(random.randrange(9))

  cadenaNumA = lambda self: self.numeroaz()+self.numeroaz()+self.numeroaz()

  encadenaNumA = lambda self: hashlib.md5(self.cadenaNumA().encode()).hexdigest()[0:3]

  ObtenerCodigo = lambda self: self.CodGrupo+self.encadenaNumA()

  CopiarCodigo = lambda self: pyperclip.copy(str(self.ObtenerCodigo()))



# codi = codicop("Holaaa")
# print(codi.ObtenerCodigo())
# codi.CopiarCodigo()